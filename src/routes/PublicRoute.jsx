import React from 'react'
import { auth } from '../utils/Auth'
import { Navigate, Outlet } from 'react-router-dom'

export const PublicRoute = () => {
    if (!auth()) {
        return <Outlet />
    }
    return <Navigate to="/about" />
}
