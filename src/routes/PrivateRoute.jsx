import React from 'react'
import { auth } from '../utils/Auth'
import { Navigate, Outlet } from 'react-router-dom'

export const PrivateRoute = () => {
    if (!auth()) {
        return <Navigate to="/login" />
    }
    return <Outlet />
}
