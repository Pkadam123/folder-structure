import './App.css'
import { Routes, Route } from "react-router-dom"
import { Sidebar } from './components/shared/sidebar/Sidebar'
import { NAV } from './utils/constants'
import { PrivateRoute } from './routes/PrivateRoute'
import { PublicRoute } from './routes/PublicRoute'
function App() {
  return (
    <>
      <Routes>
        <Route element={<Sidebar />}>
          {/* protected routes */}

          {/* <Route element={<PrivateRoute />}> */}
          {
            NAV.filter((e) => e.private).map((el) => {
              return <Route path={el.path} element={el.element} >
              </Route>
            })
          }
          {
            NAV.filter((e) => e.private).map((p) => p.children.length !== 0 ? p.children.map((m) => <Route path={`${p.path}/${m.path}`} element={m.element} />) : null)
          }

          {/* </Route> */}
          {/* public routes */}
          <Route element={<PublicRoute />}>
            {
              NAV.filter((e) => !e.private).map((el) => {
                return <Route path={el.path} element={el.element} />
              })
            }
            {
              NAV.filter((e) => !e.private).map((p) => p.children.length !== 0 ? p.children.map((m) => <Route path={`${p.path}/${m.path}`} element={m.element} />) : null)
            }
          </Route>

        </Route>
      </Routes>


    </>
  )
}

export default App
