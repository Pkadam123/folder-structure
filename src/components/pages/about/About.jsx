import React, { useEffect, useState } from 'react'
import Table from '../../shared/table/Table'
import axios from 'axios'

export const About = () => {
    const [data, setData] = useState([])
    useEffect(() => {
        let call = async () => {
            let data = await axios.get("https://64ccec742eafdcdc851a7719.mockapi.io/users")
            setData(data.data)
        }
        call()
    }, [])
    const columns = [
        {
            name: 'Name',
            selector: row => row.name,
        },
        {
            name: 'Email',
            selector: row => row.email,
        },
        {
            name: 'Phone',
            selector: row => row.phone
        }
    ];
    return (
        <div>

            <Table columns={columns} data={data} />
        </div>
    )
}
