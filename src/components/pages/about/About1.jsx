import React, { useReducer, useState } from 'react'

export const About1 = () => {

    const reducer = (state, action) => {
        switch (action.type) {
            case "INC":
                return { count: state.count++ }

            case "DEC":
                return { count: state.count-- }

            default:
                return { count: state.count }
        }
    }
    const initialstate = { count: 0 }
    const [state, dispatch] = useReducer(reducer, initialstate)
    return (
        <div>
            <button onClick={() => dispatch({ type: "INC" })}>Increment</button> {state.count} <button onClick={() => dispatch({ type: "DEC" })}>Decrement</button>
        </div>
    )
}
