import React, { useEffect, useRef, useState } from 'react'

export const Contact = () => {
    const [input, setInput] = useState('')
    const count = useRef(1)
    const inputRef = useRef()
    function handlechange(e) {
        setInput(e.target.value)
    }
    function handleclick() {
        if (!inputRef.current.value) inputRef.current.focus()
        else alert(inputRef.current.value)
    }
    useEffect(() => {
        count.current += 1
    })

    return (
        <div>
            <Input
                type='text'
                ref={inputRef}
                onChange={(e) => handlechange(e)}
                value={input} />
            <div>{input}</div>
            <div>Rendered{count.current}</div>
            <button onClick={handleclick}>Submit</button>
        </div>
    )
}

const Input = React.forwardRef((props, ref) => {
    return (
        <>
            <input
                {...props}
                ref={ref} />
        </>
    )
})