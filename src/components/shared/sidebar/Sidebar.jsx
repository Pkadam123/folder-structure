import React, { Suspense, useEffect, useState } from 'react'
import { Link, Outlet, useLocation } from 'react-router-dom'
import styles from "./Sidebar.module.scss"
import { NAV } from '../../../utils/constants'
import bar from "../../../assets/bar.png"

export const Sidebar = () => {
    const [nav, setNav] = useState(true)
    const [show, setShow] = useState(false)
    const location = useLocation()
    function click(path) {
        if (location.pathname == path[0]) {
            setShow(!show)
        } else {
            setShow(false)
        }
    }
    return (
        <>
            <div className={styles.mainContainer}>
                <div className={nav ? styles.sidebar : styles.close}>
                    <div className={styles.bar_container}>
                        <img src={bar} onClick={() => setNav(!nav)} className={styles.bar_icon} />
                    </div>
                    <ul className={styles.ul}>
                        {
                            NAV.map((e) => {
                                return <>
                                    {
                                        nav ? <Link to={e.path}><li id={e.id} onClick={() => click(e.children.map((g) => g.show))}><img src={e.icon} className={styles.nav_icon} />{e.page}</li></Link> : <Link to={e.path}><li><img src={e.icon} className={styles.nav_icon} /></li></Link>
                                    }
                                    {
                                        e.children.length !== 0 && show ? e.children.map((m) => location.pathname == m.show || location.pathname == `${e.path}/${m.show}` ? <Link to={`${e.path}/${m.path}`}><li><img src={e.icon} className={styles.nav_icon} />{m.page}</li></Link> : null) : null
                                    }
                                </>
                            })
                        }
                    </ul>
                </div>
                <div className={nav ? styles.content : styles.close_content}>
                    <Outlet />
                </div>
            </div>
        </>
    )
}
