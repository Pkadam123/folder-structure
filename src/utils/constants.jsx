import { lazy } from "react"
import home from ".././assets/home.png"
import bar from ".././assets/bar.png"
import about from ".././assets/about.png"
import phone from ".././assets/phone.png"
import login from ".././assets/login.png"
import { Contact } from "../components/pages/contact/Contact"
import { About } from "../components/pages/about/About"
import { Login } from "../components/pages/login/Login"
import { Phone } from "../components/pages/contact/Phone"
import { Home } from "../components/pages/Home/Home"
import { About1 } from "../components/pages/about/About1"
import About2 from "../components/pages/about/About2"
import Todo from "../components/pages/todo/Todo"
import { Todo1 } from "../components/pages/todo/Todo1"
import Todo2 from "../components/pages/todo/Todo2"
export const NAV = [
    {
        id: 1,
        path: "/contact",
        page: "Contact",
        element: <Contact />,
        private: true,
        icon: phone,
        children: [
            {
                path: "phone1",
                page: "Phone",
                element: <Phone />,
                show: "/contact"
            },
            {
                path: "phone2",
                page: "Phone2",
                element: <Phone />,
                show: "/contact"
            }
        ]
    },
    {
        id: 2,
        path: "/",
        page: "Home",
        element: <Home />,
        icon: home,
        children: []
    },
    {
        id: 3,
        path: "/login",
        page: "Login",
        element: <Login />,
        icon: login,
        children: []
    },
    {
        id: 4,
        path: "/about",
        page: "About",
        element: <About />,
        private: true,
        icon: about,
        children: [
            {
                path: "about1",
                page: "About1",
                element: <About1 />,
                show: "/about"
            },
            {
                path: "about2",
                page: "About2",
                element: <About2 />,
                show: "/about"
            }
        ]
    },
    {
        id: 5,
        path: "/todo",
        page: "Todo",
        element: <Todo />,
        icon: login,
        children: [
            {
                path: "todo1",
                page: "Todo1",
                element: <Todo1 />,
                show: "/todo"
            },
            {
                path: "todo2",
                page: "Todo2",
                element: <Todo2 />,
                show: "/todo"
            }
        ]

    }

]